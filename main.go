package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
	"unicode/utf8"

	"github.com/integrii/flaggy"
)

var filepath = ""

func readLines(path string) []string {
	res := []string{}
	f, err := os.Open(filepath)
	if err != nil {
		log.Fatal("ファイル開けないよ")
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		res = append(res, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return res
}

func getBlackList() [](*regexp.Regexp) {
	res := [](*regexp.Regexp){}
	patterns := []string{
		"^ *\\\\",
		"^ *-",
	}
	for _, pattern := range patterns {
		re, err := regexp.Compile(pattern)
		if err != nil {
			log.Fatalf("正規表現 %v に誤り", pattern)
		}
		res = append(res, re)
	}
	return res
}

func removeUnnecessary(lines []string) []string {
	res := []string{}
	blacklist := getBlackList()
	blackFlag := false
	for _, line := range lines {
		for _, black := range blacklist {
			if black.Match([]byte(line)) {
				blackFlag = true
				break
			}
		}
		if blackFlag {
			blackFlag = false
			continue
		}
		res = append(res, line)
	}
	return res
}

func count(lines []string) int {
	res := 0
	for _, line := range lines {
		line = strings.Trim(line, "\n")
		res += utf8.RuneCountInString(line)
	}
	return res
}

func main() {
	flaggy.String(&filepath, "f", "file", "file to count multi byte string")
	flaggy.Parse()
	lines := readLines(filepath)
	lines = removeUnnecessary(lines)
	cnt := count(lines)
	fmt.Printf("%v 文字 書きました! 😍 ", cnt)
}
